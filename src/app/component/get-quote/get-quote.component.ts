import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

let quoteID;

@Component({
  selector: 'app-get-quote',
  templateUrl: './get-quote.component.html',
  styleUrls: ['./get-quote.component.css']
})
export class GetQuoteComponent implements OnInit {
  httpOptions = {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    }
  };
  mesenger = '';
  btn_add = false;
  base_url = 'http://localhost:8081';
  title = 'Login';
  formShipment: FormGroup;
  submitted = false;
  loading = false;
  loadingAdd = false;
  addSuccess = false;
  returnUrl = '/shipment/get-quote';

  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient) { }


  get f() {
    return this.formShipment.controls;
  }

  ngOnInit() {
    this.formShipment = this.formBuilder.group({
      email: ['sender@gmail.com', [Validators.required, Validators.email]],
      name: ['sender 1', [Validators.required]],
      phone: ['0123456789', [Validators.required]],
      locality: ['Anzin', [Validators.required]],
      postal_code: ['59410', [Validators.required]],
      country_code: ['FR', [Validators.required]],
      address_line1: ['Rue Jean Jaures', [Validators.required]],
      receiver_name: ['receiver name 2', [Validators.required]],
      receiver_email: ['receiver@gmail.com', [Validators.required]],
      receiver_phone: ['037123456', [Validators.required]],
      receiver_locality: ['Marseille', [Validators.required]],
      receiver_postal_code: ['13006', [Validators.required]],
      receiver_country_code: ['FR', [Validators.required]],
      receiver_address_line1: ['175 Rue de Rome ', [Validators.required]],
      height: ['15', [Validators.required]],
      width: ['15', [Validators.required]],
      length: ['15', [Validators.required]],
      unit_cm: ['cm', [Validators.required]],
      unit: ['g', [Validators.required]],
      weight: ['1000', [Validators.required]],
    });
  }

  getQuote() {
    this.submitted = true;
    this.loading = true;
    if (this.formShipment.invalid) {
      this.loading = false;
      return;
    }
    const shipment = this.formShipment.value;

    let body = this.getBody(this.shipment, shipment);
    this.shipment = body.data;
    const url = this.base_url + "/api/shipment/getquote";
    this.http.post(url, body, this.httpOptions).subscribe(res => {
      let data: any = {};
      let mess = res['data'][0].amount;
      quoteID = res['data'][0].id;
      Number(mess);
      if (mess > 0) {
        this.btn_add = true;
        this.mesenger = ` Cost= ${res['data'][0].amount} USD
                                 Please click "Create Shipment"
                                 `;
        this.btn_add = true;
      } else {
        this.btn_add = false;
        this.mesenger = `Wrong`;
      }
      this.loading = false;
    });
  }

  createShipment() {
    this.submitted = true;
    this.loadingAdd = true;
    if (this.formShipment.invalid) {
      this.loadingAdd = false;
      return;
    }
    const shipmentInput = this.formShipment.value;
    console.log(shipmentInput);
    let body = this.getBody(this.shipment, shipmentInput);
    this.shipment = body.data;
    console.log(body);
    const url = this.base_url + "/api/shipment/createshipment";
    this.http.post(url, body, this.httpOptions)
      .subscribe(res => {

        let data: any = {};
        //   data = res['data'];
        console.log(res);
        let mess = res['data'].cost;

        Number(mess);
        console.log(mess);
        if (mess > 0) {
          this.mesenger = "Create shipment success";
        } else {
          this.mesenger = "Create shipment Error";
        }
        setTimeout(() => {
          this.loadingAdd = false;
          this.router.navigate([this.returnUrl]);
        }, 2000);
      });
  }

  backGetQuote() {
    this.btn_add = false;
  }
  getBody(body, shipment) {
    body.quote.id = quoteID;
    body.origin.contact.name = shipment.name;
    body.origin.contact.email = shipment.email;
    body.origin.contact.phone = shipment.phone;

    body.origin.address.country_code = shipment.country_code;
    body.origin.address.locality = shipment.locality;
    body.origin.address.postal_code = shipment.postal_code;
    body.origin.address.address_line1 = shipment.address_line1;

    body.destination.contact.name = shipment.receiver_name;
    body.destination.contact.email = shipment.receiver_email;
    body.destination.contact.phone = shipment.receiver_phone;

    body.destination.address.country_code = shipment.receiver_country_code;
    body.destination.address.locality = shipment.receiver_locality;
    body.destination.address.postal_code = shipment.receiver_postal_code;
    body.destination.address.address_line1 = shipment.receiver_address_line1;

    body.package.dimensions.height = shipment.height;
    body.package.dimensions.width = shipment.width;
    body.package.dimensions.length = shipment.length;
    body.package.dimensions.unit = shipment.unit_cm;

    body.package.grossWeight.amount = shipment.weight;
    body.package.grossWeight.unit = shipment.unit;

    return {
      "data": body
    };
  }

  shipment = {
    quote: {
      id: { type: String }
    },
    origin: {
      contact: {
        name: { type: String },
        email: { type: String },
        phone: { type: String },
      },
      address: {
        country_code: { type: String },
        locality: { type: String },
        postal_code: { type: Number, default: 0 },
        address_line1: { type: String },
      }
    },
    destination: {
      contact: {
        name: { type: String },
        email: { type: String },
        phone: { type: String },
      },
      address: {
        country_code: { type: String },
        locality: { type: String },
        postal_code: { type: Number, default: 0 },
        address_line1: { type: String }
      }
    },
    package: {
      dimensions: {
        height: { type: Number, default: 0 },
        width: { type: Number, default: 0 },
        length: { type: Number, default: 0 },
        unit: { type: String }
      },
      grossWeight: {
        amount: { type: Number, default: 0 },
        unit: { type: String },
      }
    },
    ref: { type: String },
    cost: { type: String },
  };


}
