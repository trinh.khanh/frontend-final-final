import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetShipmentComponent } from './get-shipment.component';

describe('GetShipmentComponent', () => {
  let component: GetShipmentComponent;
  let fixture: ComponentFixture<GetShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
