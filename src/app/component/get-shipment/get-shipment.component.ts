import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-get-shipment',
  templateUrl: './get-shipment.component.html',
  styleUrls: ['./get-shipment.component.css']
})
export class GetShipmentComponent implements OnInit {

  constructor(private route: ActivatedRoute , private http: HttpClient) { }
  ref : string;
  shipment : any;
  base_url = 'http://localhost:8081';

  ngOnInit() {
    this.ref = this.route.snapshot.paramMap.get('ref');
    console.log(this.ref);
    const url = this.base_url + "/api/shipment/getShipment/"+this.ref;
    this.http.get(url)
        .subscribe(res => {
          console.log(res);
          this.shipment = res['data'];

          console.log(this.shipment);
        });
  }

}
