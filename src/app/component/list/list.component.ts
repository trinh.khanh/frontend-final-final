import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import * as $ from 'jquery';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  base_url = 'http://localhost:8081';
  listShipment;
  ref = '';
  returnUrl = '/shipment/list';
  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient) {  }

  ngOnInit() {
    const url = this.base_url + "/api/shipment/getShipmentList";
    this.http.get(url).subscribe(res => {
      this.listShipment = res['data'];
      console.log(this.listShipment), 222222;
    });
  }
  deleteShipment() {
    const url = this.base_url + "/api/shipment/delete/"+this.ref;
    this.http.delete(url).subscribe(res => {
      console.log(res['data']);
      this.ref = '';
      window.location.reload();
    });
  }

  cancelDeleteShipment() {
    this.ref = '';
  }

  comfirmDelete(ref) {
    this.ref = ref;
    
    console.log(this.ref);
   
  }
}
