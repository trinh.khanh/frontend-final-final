import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  router = 'HomePage';

  ngOnInit() {
    this.router = 'HomePage'
  }
  changeRouter(router) {
    console.log(router);
    this.router = router;
  }


}
