import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { GetQuoteComponent } from './component/get-quote/get-quote.component';
import { GetShipmentComponent } from './component/get-shipment/get-shipment.component';
import { ListComponent } from './component/list/list.component';


const appRoutes: Routes = [
  {path: 'shipment/detail/:ref', component: GetShipmentComponent},
  {path: 'shipment/get-quote', component: GetQuoteComponent},
  {path: 'shipment/list', component: ListComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GetQuoteComponent,
    GetShipmentComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
